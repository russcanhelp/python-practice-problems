# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

# def has_quorum(attendees_list, members_list):
#     if attendees_list >= members_list / 2:
#         return True
#     else:
#         return False

# var = has_quorum(1,1)
# print(var)


def has_quorum(attendees_list, members_list):
    if len(attendees_list) >= (len(members_list) / 2):
        return True
    else:
        return False

list1 = ["dog", "cat"]
list2 = ["monkey", "rat","dog", "cat","dog", "cat"]
var = has_quorum(list1,list2)
print(var)