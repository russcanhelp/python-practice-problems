# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):

    total = sum(values)
    length = len(values)
    avg = total / length

    if avg == 0:
        return "None"
    else:
        return avg

# nums = [2,2]
# length = len(nums)
# avg = length / 2
lis = [10,10]
var = calculate_average(lis)
print(var)