# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    # mt_list = []
    # mt_list2 = []
    for x in values:
        # if x is max(values):
        #     mt_list.append(values)
        # if values == mt_list2:
        #     return "None"
        if x is max(values):
            values.remove(x)

    if values == []:
        return "None"
    else:
        return max(values)

nums = [1,2,3,4,5]
nums = find_second_largest(nums)
print(nums)
# // Task: We need to find the second largest number in the list
# // Start: We need to start off by looping through the list
#   // to find the highest value in the list and removing it
#   // then once removed we have to find the highest value from
#   // that list.
#
#
