# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    total = sum(values)
    if total == 0:
        return "None"
    else:
        return total


list1 = []
var = calculate_sum(list1)
print(var)
