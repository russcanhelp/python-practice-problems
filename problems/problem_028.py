# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    single = []
    for letter in s:
        if letter not in single:
            single.append(letter)

    complete_str = ""
    complete_str = complete_str.join(single)

  # // Will return letters in one string
    #     elif s == "":
    #         return s
    # no_dup = ""
    # no_dup = no_dup.join(single)

    return complete_str
    # dup_letters = set(s)
    # letters = "".join(dup_letters)
    # return letters




var = remove_duplicate_letters("aabbccddeeffgg")
print(var)
