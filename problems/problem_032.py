# Complete the sum_of_first_n_numbers function which accepts
# a numerical limit and returns the sum of the numbers from
# 0 up to and including limit.
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+1=1
#   * 2 returns 0+1+2=3
#   * 5 returns 0+1+2+3+4+5=15
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# // Task: Complete a function which gets the sum of numbers
    # // But if the value of the number in the list is
    # // less than 0 (negative) it should auto return none

# // Start: for Loop the numbers in the list
    # // If loop comes across a num < 0
        # // return "None"
    # // Else if the value of the numbers is greater than 0
        # add to a list
    # // get the sum of the list


def sum_of_first_n_numbers(limit):

    # real_nums = []
    neg_list = []

    for x in limit:
        if x < 0:
            neg_list.append(x)
        # elif x >= 0:
        #     real_nums.append(x)

    if neg_list != []:
        return "None"
    else:
        return sum(limit)


real_nums = [1,2,0,-1]
real_nums = sum_of_first_n_numbers(real_nums)
print(real_nums)

# Actual solution:
    # // looped list
        # // if x was less than 0 add to a list
        # // else is x is greater than or equal to 0 add to seperate list
    # // if the the first list was not equal to empty list return "None"
        # // else get the sum of nums on the ther list

# Shortend Version:
    # // looped list
        # //if x is less than 0 add to list
    # //if list is not empty return "None
    # //else return sum of list