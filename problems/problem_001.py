# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2):
    zero = 0
    first_value = value1 + 0
    second_value = value2 + 0
    # if first_value is second_value:
    #     return("Value is the same")
    if first_value < second_value:
        return(first_value, "is lower")
    elif first_value > second_value:
        return(second_value, "is lower")
    else:
        return("they're even")

values = minimum_value(5,4)
print(values)