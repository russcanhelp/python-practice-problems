# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# // Task: Add the value of each squared item
#   // If list is empty, return None.

# // If list is equal to empty then return "None"
# // for loop each value and square them
# // once they're all square return the sum of the whole list



def sum_of_squares(values):

    squared = []

    for x in values:
        squared.append(x ** 2)

    if values == []:
        return "None"
    else:
        return sum(squared)

nums = []
nums = sum_of_squares(nums)
print(nums)


# // Solution
    # // loop list and add squared values to list
    # // if list is empty return "None"
    # // else return the sum of the list. 